﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;
using SVOAScheduling.Services;

namespace SVOAScheduling.Controllers
{
    [Authorize(Roles = ("Admin, System Administrator"))]
    public class RefereeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RefereeController(ApplicationDbContext context)
        {
            _context = context;
        }

        //Get List of Refs
        public async Task<IActionResult> Index(bool? status)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var userSettings = RefereeServices.GetSiteSettings(_context, userId);
            status = (status == null) ? true : status;
            var referees = _context.Profiles
                .Include(p => p.User)
                .Include(p => p.UserSettings)
                .ThenInclude(us => us.Level)
                .Where(p => p.UserSettings.AssociationId == userSettings.AssociationId && p.UserSettings.Status == status);

            return View(await referees.ToListAsync());

        }

    }
}
