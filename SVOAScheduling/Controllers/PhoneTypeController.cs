using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Controllers
{
    [Authorize(Roles = ("Admin, System Administrator"))]
    public class PhoneTypeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PhoneTypeController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PhoneType
        public async Task<IActionResult> Index()
        {
            return View(await _context.PhoneTypes.ToListAsync());
        }

        // GET: PhoneType/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var phoneType = await _context.PhoneTypes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (phoneType == null)
            {
                return NotFound();
            }

            return View(phoneType);
        }

        // GET: PhoneType/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PhoneType/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description")] PhoneType phoneType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(phoneType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(phoneType);
        }

        // GET: PhoneType/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var phoneType = await _context.PhoneTypes.FindAsync(id);
            if (phoneType == null)
            {
                return NotFound();
            }
            return View(phoneType);
        }

        // POST: PhoneType/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description")] PhoneType phoneType)
        {
            if (id != phoneType.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(phoneType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PhoneTypeExists(phoneType.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(phoneType);
        }

        // GET: PhoneType/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var phoneType = await _context.PhoneTypes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (phoneType == null)
            {
                return NotFound();
            }

            return View(phoneType);
        }

        // POST: PhoneType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var phoneType = await _context.PhoneTypes.FindAsync(id);
            _context.PhoneTypes.Remove(phoneType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PhoneTypeExists(int id)
        {
            return _context.PhoneTypes.Any(e => e.Id == id);
        }
    }
}
