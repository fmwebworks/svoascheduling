using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Controllers
{
    [Authorize(Roles = ("Admin, System Administrator"))]
    public class LeagueTypeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LeagueTypeController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: LeagueType
        public async Task<IActionResult> Index()
        {
            return View(await _context.LeagueTypes.ToListAsync());
        }

        // GET: LeagueType/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leagueType = await _context.LeagueTypes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (leagueType == null)
            {
                return NotFound();
            }

            return View(leagueType);
        }

        // GET: LeagueType/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LeagueType/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description")] LeagueType leagueType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(leagueType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(leagueType);
        }

        // GET: LeagueType/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leagueType = await _context.LeagueTypes.FindAsync(id);
            if (leagueType == null)
            {
                return NotFound();
            }
            return View(leagueType);
        }

        // POST: LeagueType/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description")] LeagueType leagueType)
        {
            if (id != leagueType.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(leagueType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LeagueTypeExists(leagueType.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(leagueType);
        }

        // GET: LeagueType/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leagueType = await _context.LeagueTypes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (leagueType == null)
            {
                return NotFound();
            }

            return View(leagueType);
        }

        // POST: LeagueType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var leagueType = await _context.LeagueTypes.FindAsync(id);
            _context.LeagueTypes.Remove(leagueType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LeagueTypeExists(int id)
        {
            return _context.LeagueTypes.Any(e => e.Id == id);
        }
    }
}
