﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SVOAScheduling.Models;
using SVOAScheduling.Models.ViewModels;
using SVOAScheduling.Data;
using SVOAScheduling.Services;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace SVOAScheduling.Controllers
{
    [Authorize(Roles = ("Referee, Assignor, Admin, Super Administrator"))]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;


        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var games = GameServices.GetGamesByUser(_context, userId);

                List<myScheduleVM> schedule = new List<myScheduleVM>();

                foreach (Game game in games)
                {
                    myScheduleVM sch = new myScheduleVM()
                    {
                        Location = game.Location.Name,
                        MatchDate = game.Date,
                        MatchId = game.Id,
                        MatchNumber = game.Number,
                        HomeTeam = game.HomeTeam.Abbreviation,
                        VisitingTeam = game.VisitingTeam.Abbreviation,
                    };
                    if (game.GameAssignments.Any(ga => ga.Type.Description =="R1"))
                    {
                        sch.R1 = game.GameAssignments.FirstOrDefault(ga => ga.Type.Description == "R1").Official.FullName;
                    }

                    if(game.GameAssignments.Any(ga => ga.Type.Description =="R2"))
                    {
                        sch.R2 = game.GameAssignments.FirstOrDefault(ga => ga.Type.Description == "R2").Official.FullName;
                    }

                    schedule.Add(sch);
                };

                return View(schedule.AsEnumerable());
            }
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
