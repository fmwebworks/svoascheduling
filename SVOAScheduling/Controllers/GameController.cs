using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;
using SVOAScheduling.Models.ViewModels;
using SVOAScheduling.Services;

namespace SVOAScheduling.Controllers
{
    [Authorize(Roles = "Referee, Assignor, Admin, Super Administrator")]
    public class GameController : Controller
    {
        private readonly ApplicationDbContext _context;

        public GameController(ApplicationDbContext context)
        {
            _context = context;

        }

        // GET: Game
        public IActionResult Index()
        {

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var leagueId = RefereeServices.GetRefereeLeagueId(userId, _context);
            var seasonId = SeasonServices.GetCurrentSeason(_context, leagueId);
            var games = GameServices.GetGamesBySeason(_context, seasonId);

            var gameCount = games.Count();
            if (gameCount > 0)
            {
                var schedule = new List<FullScheduleViewModel>();

                foreach (Game game in games)
                {
                    FullScheduleViewModel sch = new FullScheduleViewModel();

                    sch.Id = game.Id;
                    sch.Location = game.Location.Name;
                    sch.Date = game.Date;
                    sch.Division = game.Division.Name;
                    sch.HomeTeam = game.HomeTeam.Abbreviation;
                    sch.VisitingTeam = game.VisitingTeam.Abbreviation;
                    sch.Number = game.Number;
                    sch.R1 = (game.GameAssignments.Any(ga => ga.Type.Description == "R1")) ? game.GameAssignments.FirstOrDefault(ga => ga.Type.Description == "R1").Official.FullName : "";
                    sch.R2 = (game.GameAssignments.Any(ga => ga.Type.Description == "R2")) ? game.GameAssignments.FirstOrDefault(ga => ga.Type.Description == "R2").Official.FullName : "";
                   

                    schedule.Add(sch);
                }


                return View(schedule.AsEnumerable());
            }
            return View(new List<FullScheduleViewModel>().AsEnumerable());
        }

        // GET: Game/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _context.Games
                .Include(g => g.Division)
                .Include(g => g.Format)
                .Include(g => g.Location)
                .Include(g => g.Payment)
                .Include(g => g.Season)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (game == null)
            {
                return NotFound();
            }

            return View(game);
        }

        // GET: Game/Create
        [Authorize(Roles = ("Assignor, Administrator"))]
        public IActionResult Create()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var siteSettings = RefereeServices.GetSiteSettings(_context, userId);
            var date = DateTime.Now;

            Game game = new Game()
            {
                IsActive = true,
                SeasonId = siteSettings.SeasonId,
                Date = new DateTime(date.Year, date.Month, date.Day, 17, 0, 0)
            };

            var locations = LocationServices.GetLocations(_context, siteSettings.OrganziationId);
            ViewData["DivisionId"] = new SelectList(DivisionServices.GetDivisions(_context, siteSettings.LeagueId), "Id", "Name");
            ViewData["FormatId"] = new SelectList(_context.Format, "Id", "Description");
            ViewData["LocationId"] = new SelectList(locations, "Id", "Name");
            ViewData["PaymentId"] = new SelectList(PaymentServices.GetPayments(_context, siteSettings.LeagueId), "Id", "Price");
            ViewData["SeasonId"] = new SelectList(SeasonServices.GetSeasons(_context, siteSettings.LeagueId), "Id", "Name");
            ViewData["HomeId"] = new SelectList(locations, "Id", "Name");
            ViewData["VisitorsId"] = new SelectList(locations, "Id", "Name");

            return View(game);
        }

        // POST: Game/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = ("Assignor, Administrator"))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,LocationId,SeasonId,Date,PaymentId,IsActive,FormatId,DivisionId,HomeTeamId,VisitingTeamId,Number")] Game game)
        {
            if (ModelState.IsValid)
            {
                _context.Add(game);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var siteSettings = RefereeServices.GetSiteSettings(_context, userId);
            var locations = LocationServices.GetLocations(_context, siteSettings.OrganziationId);

            ViewData["DivisionId"] = new SelectList(DivisionServices.GetDivisions(_context, siteSettings.LeagueId), "Id", "Name", game.DivisionId);
            ViewData["FormatId"] = new SelectList(_context.Format, "Id", "Description", game.FormatId);
            ViewData["LocationId"] = new SelectList(locations, "Id", "Name", game.LocationId);
            ViewData["PaymentId"] = new SelectList(PaymentServices.GetPayments(_context, siteSettings.LeagueId), "Id", "Price", game.PaymentId);
            ViewData["SeasonId"] = new SelectList(SeasonServices.GetSeasons(_context, siteSettings.LeagueId), "Id", "Name", game.SeasonId);
            ViewData["HomeId"] = new SelectList(locations, "Id", "Name", game.HomeTeamId);
            ViewData["VisitorsId"] = new SelectList(locations, "Id", "Name", game.VisitingTeamId);
            return View(game);
        }

        // GET: Game/Edit/5
        [Authorize(Roles = ("Assignor, Administrator"))]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _context.Games.FindAsync(id);
            if (game == null)
            {
                return NotFound();
            }
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var siteSettings = RefereeServices.GetSiteSettings(_context, userId);
            var locations = LocationServices.GetLocations(_context, siteSettings.OrganziationId);

            ViewData["DivisionId"] = new SelectList(DivisionServices.GetDivisions(_context, siteSettings.LeagueId), "Id", "Name", game.DivisionId);
            ViewData["FormatId"] = new SelectList(_context.Format, "Id", "Description", game.FormatId);
            ViewData["LocationId"] = new SelectList(locations, "Id", "Name", game.LocationId);
            ViewData["PaymentId"] = new SelectList(PaymentServices.GetPayments(_context, siteSettings.LeagueId), "Id", "Price", game.PaymentId);
            ViewData["SeasonId"] = new SelectList(SeasonServices.GetSeasons(_context, siteSettings.LeagueId), "Id", "Name", game.SeasonId);
            ViewData["HomeId"] = new SelectList(locations, "Id", "Name", game.HomeTeamId);
            ViewData["VisitorsId"] = new SelectList(locations, "Id", "Name", game.VisitingTeamId);
            return View(game);
        }

        // POST: Game/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = ("Assignor, Administrator"))]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,LocationId,SeasonId,Date,PaymentId,IsActive,FormatId,DivisionId,HomeTeamId,VisitingTeamId,Number")] Game game)
        {
            if (id != game.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(game);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GameServices.GameExists(_context, game.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var siteSettings = RefereeServices.GetSiteSettings(_context, userId);
            var locations = LocationServices.GetLocations(_context, siteSettings.OrganziationId);

            ViewData["DivisionId"] = new SelectList(DivisionServices.GetDivisions(_context, siteSettings.LeagueId), "Id", "Name", game.DivisionId);
            ViewData["FormatId"] = new SelectList(_context.Format, "Id", "Description", game.FormatId);
            ViewData["LocationId"] = new SelectList(locations, "Id", "Name", game.LocationId);
            ViewData["PaymentId"] = new SelectList(PaymentServices.GetPayments(_context, siteSettings.LeagueId), "Id", "Price", game.PaymentId);
            ViewData["SeasonId"] = new SelectList(SeasonServices.GetSeasons(_context, siteSettings.LeagueId), "Id", "Name", game.SeasonId);
            ViewData["HomeId"] = new SelectList(locations, "Id", "Name", game.HomeTeamId);
            ViewData["VisitorsId"] = new SelectList(locations, "Id", "Name", game.VisitingTeamId);

            return View(game);
        }

        // GET: Game/Delete/5
        [Authorize(Roles = ("Assignor, Administrator"))]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _context.Games
                .Include(g => g.Division)
                .Include(g => g.Format)
                .Include(g => g.Location)
                .Include(g => g.Payment)
                .Include(g => g.Season)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (game == null)
            {
                return NotFound();
            }

            return View(game);
        }

        // POST: Game/Delete/5
        [Authorize(Roles = ("Assignor, Administrator"))]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var game = await _context.Games.FindAsync(id);
            _context.Games.Remove(game);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}
