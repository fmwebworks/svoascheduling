using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Controllers
{
    [Authorize(Roles = ("Admin, System Administrator"))]
    public class AssignmentTypeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AssignmentTypeController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: AssignmentType
        public async Task<IActionResult> Index()
        {
            return View(await _context.AssignmentTypes.ToListAsync());
        }

        // GET: AssignmentType/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignmentType = await _context.AssignmentTypes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (assignmentType == null)
            {
                return NotFound();
            }

            return View(assignmentType);
        }

        // GET: AssignmentType/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AssignmentType/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description")] AssignmentType assignmentType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(assignmentType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(assignmentType);
        }

        // GET: AssignmentType/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignmentType = await _context.AssignmentTypes.FindAsync(id);
            if (assignmentType == null)
            {
                return NotFound();
            }
            return View(assignmentType);
        }

        // POST: AssignmentType/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description")] AssignmentType assignmentType)
        {
            if (id != assignmentType.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(assignmentType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AssignmentTypeExists(assignmentType.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(assignmentType);
        }

        // GET: AssignmentType/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignmentType = await _context.AssignmentTypes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (assignmentType == null)
            {
                return NotFound();
            }

            return View(assignmentType);
        }

        // POST: AssignmentType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var assignmentType = await _context.AssignmentTypes.FindAsync(id);
            _context.AssignmentTypes.Remove(assignmentType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AssignmentTypeExists(int id)
        {
            return _context.AssignmentTypes.Any(e => e.Id == id);
        }
    }
}
