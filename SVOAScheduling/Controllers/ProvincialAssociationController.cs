using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Controllers
{
    [Authorize(Roles = ("Admin, System Administrator"))]
    public class ProvincialAssociationController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProvincialAssociationController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ProvincialAssociation
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ProvincialAssociations.Include(p => p.Province).Include(p => p.Sport);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ProvincialAssociation/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provincialAssociation = await _context.ProvincialAssociations
                .Include(p => p.Province)
                .Include(p => p.Sport)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (provincialAssociation == null)
            {
                return NotFound();
            }

            return View(provincialAssociation);
        }

        // GET: ProvincialAssociation/Create
        public IActionResult Create()
        {
            ViewData["ProvinceId"] = new SelectList(_context.Provinces, "Id", "Id");
            ViewData["SportId"] = new SelectList(_context.Sports, "Id", "Id");
            return View();
        }

        // POST: ProvincialAssociation/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Term,URL,SportId,ProvinceId,Country")] ProvincialAssociation provincialAssociation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(provincialAssociation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProvinceId"] = new SelectList(_context.Provinces, "Id", "Id", provincialAssociation.ProvinceId);
            ViewData["SportId"] = new SelectList(_context.Sports, "Id", "Id", provincialAssociation.SportId);
            return View(provincialAssociation);
        }

        // GET: ProvincialAssociation/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provincialAssociation = await _context.ProvincialAssociations.FindAsync(id);
            if (provincialAssociation == null)
            {
                return NotFound();
            }
            ViewData["ProvinceId"] = new SelectList(_context.Provinces, "Id", "Id", provincialAssociation.ProvinceId);
            ViewData["SportId"] = new SelectList(_context.Sports, "Id", "Id", provincialAssociation.SportId);
            return View(provincialAssociation);
        }

        // POST: ProvincialAssociation/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Term,URL,SportId,ProvinceId,Country")] ProvincialAssociation provincialAssociation)
        {
            if (id != provincialAssociation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(provincialAssociation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProvincialAssociationExists(provincialAssociation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProvinceId"] = new SelectList(_context.Provinces, "Id", "Id", provincialAssociation.ProvinceId);
            ViewData["SportId"] = new SelectList(_context.Sports, "Id", "Id", provincialAssociation.SportId);
            return View(provincialAssociation);
        }

        // GET: ProvincialAssociation/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var provincialAssociation = await _context.ProvincialAssociations
                .Include(p => p.Province)
                .Include(p => p.Sport)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (provincialAssociation == null)
            {
                return NotFound();
            }

            return View(provincialAssociation);
        }

        // POST: ProvincialAssociation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var provincialAssociation = await _context.ProvincialAssociations.FindAsync(id);
            _context.ProvincialAssociations.Remove(provincialAssociation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProvincialAssociationExists(int id)
        {
            return _context.ProvincialAssociations.Any(e => e.Id == id);
        }
    }
}
