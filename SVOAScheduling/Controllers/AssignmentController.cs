using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;
using SVOAScheduling.Models.ViewModels;
using SVOAScheduling.Services;
using Microsoft.AspNetCore.Authorization;

namespace SVOAScheduling.Controllers
{
    [Authorize(Roles = "Assignor, Admin, Super Administrator")]
    public class AssignmentController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AssignmentController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Assignment/Modify
        public IActionResult Modify(int id)
        {
            Game game = GameServices.GetGameById(_context, id);
            if (game is null)
            {
                return NotFound();
            }
            var affiliatedGames = GameServices.GetAffiliatedGames(_context, game);

            GameViewModel match = GameServices.PopulateGameViewModel(game, affiliatedGames, false);
            
            var assignmentType = _context.AssignmentTypes.Where(at => at.SportId ==1).OrderBy(at => at.SortOrder);

            foreach(var type in assignmentType)
            {
                var officialId = match.Assignments.FirstOrDefault(a => a.AssignmentTypeId == type.Id);
                if (officialId == null)
                {
                    var newType = new AssignmentViewModel()
                    {
                        AssignmentTypeId = type.Id,
                        AssignmentText = type.Description,
                        Gym = "",
                        OfficialId = "",
                        AssignmentId = 0
                    };
                    match.Assignments.Add(newType);
                }
            }
 
            ViewData["OfficialId"] = new SelectList(_context.Profiles, "UserId", "FullName");
            return View(match);
        }

        // POST: Assignment/Modify
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Modify(GameViewModel match)
        {
            if (ModelState.IsValid)
            {
                var r1 = _context.AssignmentTypes.AsNoTracking().FirstOrDefault(at => at.Description == "R1").Id;
                var r2 = _context.AssignmentTypes.AsNoTracking().FirstOrDefault(at => at.Description == "R2").Id;
                foreach (AssignmentViewModel assignment in match.Assignments)
                {
                    if (assignment.OfficialId is not null)
                    {
                        var assignmentType = assignment.AssignmentTypeId;
                        Assignment role = new Assignment()
                        {
                            OfficialId = assignment.OfficialId,
                            Gym = assignment.Gym,
                            AssignmentTypeId = assignment.AssignmentTypeId,
                            GameId = match.GameId
                        };

                        if (assignment.OfficialId == "0")
                        {
                            role.Id = assignment.AssignmentId;
                            _context.Assignments.Remove(role);
                        }
                        else if(assignment.AssignmentId != 0)
                        {
                            role.Id = assignment.AssignmentId;
                            _context.Update(role);
                        }
                        else
                        {
                            _context.Add(role);
                        }

                        if (match.AffiliatedGames.Any(ag => ag.Checked))
                        {
                            foreach (AffiliatedGameViewModel affiliated in match.AffiliatedGames)
                            {
                                var gameId = affiliated.GameId;

                                if (affiliated.Reverse)
                                {
                                    assignmentType = (assignmentType == r1) ? r2 : (assignmentType == r2) ? r1 : r2;
                                }
                                else
                                {
                                    assignmentType = assignment.AssignmentTypeId;
                                }

                                Assignment affiliatedRoles = new Assignment()
                                {
                                    GameId = gameId,
                                    OfficialId = assignment.OfficialId,
                                    Gym = assignment.Gym,
                                    AssignmentTypeId = assignmentType
                                };

                                // check for existing assignments
                                var existingAssignments = _context.Assignments.AsNoTracking().FirstOrDefault(a => a.GameId == gameId && a.AssignmentTypeId == assignmentType);

                                if (assignment.OfficialId == "0")
                                {
                                    affiliatedRoles.Id = existingAssignments.Id;
                                    _context.Assignments.Remove(affiliatedRoles);
                                }
                                else if (existingAssignments is not null)
                                {
                                    affiliatedRoles.Id = existingAssignments.Id;
                                    _context.Update(affiliatedRoles);
                                }
                                else
                                {
                                    _context.Add(affiliatedRoles);
                                }

                            }
                        }
                    }

                }

                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Game");
            }

            Game game = _context.Games.AsNoTracking().Include(g => g.HomeTeam).Include(g => g.Location).Include(g => g.VisitingTeam).First(g => g.Id == match.Id);

            match.Locaton = game.Location.Name;
            match.VisitingTeam = game.VisitingTeam.Abbreviation;
            match.HomeTeam = game.HomeTeam.Abbreviation;
            match.Date = game.Date;

            ViewData["Officials"] = new SelectList(_context.Profiles, "Id", "FullName");
            return View(match);
        }

        // GET: Assignment/Delete/5
        public IActionResult Delete(int? id)
        {
             var game = GameServices.GetGameById(_context, id);
           if (id == null || game == null)
            {
                return NotFound();
            }

            var affiliatedGames = GameServices.GetAffiliatedGames(_context, game);

            GameViewModel match = GameServices.PopulateGameViewModel(game, affiliatedGames, true);

            if (match == null)
            {
                return NotFound();
            }

            return View(match);
        }

        // POST: Assignment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(GameViewModel match)
        {
            if (match == null)
                return NotFound();
            var countRecords = 0;
            if (match.Checked)
            {
                var removeAssign = _context.Assignments.Where(a => a.GameId == match.GameId);
                _context.Assignments.RemoveRange(removeAssign);
                countRecords = removeAssign.Count();
            }

            if(match.AffiliatedGames != null)
            {
                foreach(var game in match.AffiliatedGames)
                {
                    if (game.Checked)
                    {
                        var removeAffiliatedAssign = _context.Assignments.Where(a => a.GameId == game.GameId);
                        _context.Assignments.RemoveRange(removeAffiliatedAssign);
                        countRecords = countRecords + removeAffiliatedAssign.Count();
                    }
                }
            }

            var success = await _context.SaveChangesAsync();

            ViewBag.Success = (success == countRecords) ? true : false;

            return RedirectToAction("Modify", "Assignment", new { id=match.GameId });
        }

        private bool AssignmentExists(int id)
        {
            return _context.Assignments.Any(e => e.Id == id);
        }
    }
}
