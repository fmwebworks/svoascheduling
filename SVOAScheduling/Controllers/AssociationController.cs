using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Controllers
{
    [Authorize(Roles = ("Admin, System Administrator"))]
    public class AssociationController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AssociationController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Association
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Associations.Include(a => a.ProvincialAssociation);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Association/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var association = await _context.Associations
                .Include(a => a.ProvincialAssociation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (association == null)
            {
                return NotFound();
            }

            return View(association);
        }

        // GET: Association/Create
        public IActionResult Create()
        {
            ViewData["ProvincialAssociationId"] = new SelectList(_context.ProvincialAssociations, "Id", "Id");
            return View();
        }

        // POST: Association/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Contact,PhoneNumber,ProvincialAssociationId")] Association association)
        {
            if (ModelState.IsValid)
            {
                _context.Add(association);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProvincialAssociationId"] = new SelectList(_context.ProvincialAssociations, "Id", "Id", association.ProvincialAssociationId);
            return View(association);
        }

        // GET: Association/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var association = await _context.Associations.FindAsync(id);
            if (association == null)
            {
                return NotFound();
            }
            ViewData["ProvincialAssociationId"] = new SelectList(_context.ProvincialAssociations, "Id", "Id", association.ProvincialAssociationId);
            return View(association);
        }

        // POST: Association/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Contact,PhoneNumber,ProvincialAssociationId")] Association association)
        {
            if (id != association.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(association);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AssociationExists(association.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProvincialAssociationId"] = new SelectList(_context.ProvincialAssociations, "Id", "Id", association.ProvincialAssociationId);
            return View(association);
        }

        // GET: Association/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var association = await _context.Associations
                .Include(a => a.ProvincialAssociation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (association == null)
            {
                return NotFound();
            }

            return View(association);
        }

        // POST: Association/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var association = await _context.Associations.FindAsync(id);
            _context.Associations.Remove(association);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AssociationExists(int id)
        {
            return _context.Associations.Any(e => e.Id == id);
        }
    }
}
