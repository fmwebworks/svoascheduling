﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SVOAScheduling.Data;
using Microsoft.AspNetCore.Authorization;

namespace SVOAScheduling.Areas.Identity.Pages.Role
{
    [Authorize(Roles = ("System Administrator"))]
    public class DeleteModel : PageModel
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _context;

        public DeleteModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _context = context;
        }

        [BindProperty]
        public IdentityRole Input { get; set; }

        public async Task<IActionResult> OnGet( string id)
        {
            Input = await _roleManager.FindByIdAsync(id);
            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var refereeRole = await _roleManager.FindByNameAsync("Referee");
            var role = await _roleManager.FindByIdAsync(Input.Id);
            var userList = await _userManager.GetUsersInRoleAsync(role.Name);

            foreach (var user in userList)
            {
                var addRoleResult = await _userManager.AddToRoleAsync(user, refereeRole.Id);
                if (!addRoleResult.Succeeded)
                {
                    foreach (var error in addRoleResult.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    return Page();
                }
                var removeResult = await _userManager.RemoveFromRoleAsync(user, role.Id);
                if (!removeResult.Succeeded)
                {
                    foreach (var error in removeResult.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    return Page();
                }
            }

            var result = await _roleManager.DeleteAsync(role);

            if (result.Succeeded)
            {
                return RedirectToPage("./Index");
            }
            return Page();
        }
    }
}
