﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SVOAScheduling.Data;
using SVOAScheduling.Services;

namespace SVOAScheduling.Areas.Identity.Pages.Role
{
    [Authorize(Roles = ("Admin, System Administrator"))]
    public class ListModel : PageModel
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _context;

        public ListModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _context = context;
        }

        [BindProperty]
        public List<UserViewModel> Input { get; set; }

        public class UserViewModel
        {
            public string Id { get; set; }
            [Display(Name ="Name")]
            public string FullName { get; set; }
            [Display(Name = "Roles")]
            public List<RoleViewModel> Roles { get; set; }
            public string City { get; set; }
            public string Email { get; set; }
            
        }

        public class RoleViewModel
        {
            public string Name { get; set; }
        }

        public async Task<IActionResult> OnGet()
        {
            var users = _context.Users;
            Input = new List<UserViewModel>();
            foreach(var user in users)
            {
                var roles = await _userManager.GetRolesAsync(user);
                var profile = RefereeServices.GetProfile(_context, user.Id);

                var userModel = new UserViewModel()
                {
                    Id = user.Id,
                    FullName = profile.FullName,
                    City = profile.City,
                    Email = user.Email,
                    Roles = new List<RoleViewModel>()
                };
                if (roles != null)
                {
                    for (int i = 0; i <= roles.Count() - 1; i++)
                    {
                        var thisRole = new RoleViewModel()
                        {
                            Name = roles[i]
                        };
                        userModel.Roles.Add(thisRole);
                    }
                }
                else
                {
                    userModel.Roles.Add(null);
                }
                Input.Add(userModel);
            }

            return Page();
        }
    }
}
