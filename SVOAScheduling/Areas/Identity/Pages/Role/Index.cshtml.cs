﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace SVOAScheduling.Areas.Identity.Pages.Role
{
    [Authorize(Roles = ("System Administrator"))]
    public class IndexModel : PageModel
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        public IndexModel(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
        }

        [BindProperty]
        public IEnumerable<IdentityRole> Input { get; set; }

        public IActionResult OnGet()
        {
            Input = _roleManager.Roles;
            return Page();
        }
    }
}
