﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SVOAScheduling.Data;
using SVOAScheduling.Services;

namespace SVOAScheduling.Areas.Identity.Pages.Role
{
    [Authorize(Roles = ("Admin, System Administrator"))]
    public class AssignModel : PageModel
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _context;

        public AssignModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _context = context;
        }

        [BindProperty]
        public UserViewModel Input { get; set; }

        public class UserViewModel
        {
            public string Id { get; set; }
            [Display(Name = "Name")]
            public string FullName { get; set; }
            [Display(Name = "Roles")]
            public List<RoleViewModel> Roles { get; set; }
            public string City { get; set; }
            public string Email { get; set; }

        }

        public class RoleViewModel
        {
            public string Name { get; set; }
            public string NormalizedRoleName { get; set; }
            public string Id { get; set; }
            public bool Checked { get; set; }
        }
        
        public async Task<IActionResult> OnGet(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var roles = _roleManager.Roles;
            var profile = RefereeServices.GetProfile(_context, id);
            var userRoles = await _userManager.GetRolesAsync(user);

            Input = new UserViewModel()
            {
                Id = id,
                FullName = profile.FullName,
                City = profile.City,
                Email = user.Email,
                Roles = new List<RoleViewModel>()
            };

            foreach(var role in roles)
            {
                var addRole = new RoleViewModel()
                {
                    Id = role.Id,
                    Name = role.Name,
                    NormalizedRoleName = role.NormalizedName,
                    Checked = (userRoles.Contains(role.Name)) ? true : false
                };
                Input.Roles.Add(addRole);
            }

            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            var user = await _userManager.FindByIdAsync(Input.Id);
                var userRoles = await _userManager.GetRolesAsync(user);

            foreach(var role in Input.Roles)
            {
                bool inUserRoles = userRoles.Contains(role.Name);
                if (role.Checked && !inUserRoles)
                {
                    var addRoleResult = await _userManager.AddToRoleAsync(user, role.NormalizedRoleName);
                    if(!addRoleResult.Succeeded)
                    {
                        foreach (var error in addRoleResult.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                        return Page();

                    }
                }
                else if(!role.Checked && inUserRoles)
                {
                    var removeRoleResult = await _userManager.RemoveFromRoleAsync(user, role.NormalizedRoleName);
                    if (!removeRoleResult.Succeeded)
                    {
                        foreach (var error in removeRoleResult.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                        return Page();
                    }
                }
            }

            return RedirectToPage("./List");
        }
    }
}
