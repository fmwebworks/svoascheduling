using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SVOAScheduling.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SVOAScheduling.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using SVOAScheduling.Models;

namespace SVOAScheduling.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly ApplicationDbContext _context;

        public IndexModel(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        public string Username { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Display(Name = "First Name")]
            public string FirstName { get; set; }
            [Display(Name="Last Name")]
            public string LastName { get; set; }
            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }
            [Display(Name = "Phone Type")]
            public int PhoneTypeId { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            [Display(Name="Province")]
            public int ProvinceId { get; set; }
            [Display(Name="Postal Code")]
            [DataType(DataType.PostalCode)]
            [DisplayFormat(DataFormatString = "{0:*#* #*#}", ApplyFormatInEditMode = true)]
            public string PostalCode { get; set; }
        }

        private async Task LoadAsync(IdentityUser user)
        {
            var userName = await _userManager.GetUserNameAsync(user);
            var profile = RefereeServices.GetProfile(_context, user.Id);

            Username = userName;

            Input = new InputModel
            {
                PhoneNumber = profile.PhoneNumber,
                FirstName = profile.FirstName,
                LastName = profile.LastName,
                Address = profile.Address,
                City = profile.City,
                PostalCode = profile.PostalCode,
                ProvinceId = profile.ProvinceId,
                PhoneTypeId = profile.PhoneTypeId
            };

            ViewData["PhoneTypeId"] = new SelectList(_context.PhoneTypes, "Id", "Description", profile.PhoneTypeId);
            ViewData["ProvinceId"] = new SelectList(_context.Provinces, "Id", "Term", profile.ProvinceId);
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            var profile = RefereeServices.GetProfile(_context, user.Id);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            if (Input.PhoneNumber != profile.PhoneNumber || Input.FirstName != profile.FirstName || Input.LastName != profile.LastName || Input.PhoneTypeId != profile.PhoneTypeId
                || Input.Address != profile.Address || Input.City != profile.Address || Input.ProvinceId != profile.ProvinceId || Input.PostalCode != profile.PostalCode)
            {
                var updateProfile = new Profile() {
                    UserId = user.Id,
                    FirstName = Input.FirstName,
                    LastName = Input.LastName,
                    PhoneNumber = Input.PhoneNumber,
                    PhoneTypeId = Input.PhoneTypeId,
                    Address = Input.Address,
                    City = Input.City,
                    ProvinceId = Input.PhoneTypeId,
                    PostalCode = Input.PostalCode,
                    UserSettingsId = profile.UserSettingsId
                };

                var setProfile = RefereeServices.UpdateProfile(_context, updateProfile);
                if (!setProfile)
                {
                    StatusMessage = "Unexpected error when trying to save.";
                    return RedirectToPage();
                }
            }

            await _signInManager.RefreshSignInAsync(user);
            StatusMessage = "Your profile has been updated";
            return RedirectToPage();
        }
    }
}
