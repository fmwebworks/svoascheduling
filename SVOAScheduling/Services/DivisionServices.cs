﻿using System;
using System.Collections.Generic;
using System.Linq;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Services
{
    public class DivisionServices
    {
        public static IQueryable<Division> GetDivisions (ApplicationDbContext _context, int leagueId)
        {
            return _context.Divisions.Where(d => d.LeagueId == leagueId);
        }
    }
}
