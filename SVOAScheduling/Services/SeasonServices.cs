﻿using System;
using System.Collections.Generic;
using System.Linq;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Services
{
    public class SeasonServices
    {
        public static int GetCurrentSeason(ApplicationDbContext _context, int leagueId)
        {
            return _context.Seasons.FirstOrDefault(s => s.IsCurrent && s.LeagueId == leagueId).Id;

        }

        public static IQueryable<Season> GetSeasons(ApplicationDbContext _context, int leagueId)
        {
            return _context.Seasons.Where(s => s.LeagueId == leagueId);
        }
    }
}
