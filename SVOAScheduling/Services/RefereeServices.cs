﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;
using SVOAScheduling.Models.ViewModels;

namespace SVOAScheduling.Services
{
    public class RefereeServices 
    {
        public static int GetRefereeLeagueId(string userId, ApplicationDbContext _context)
        {
            UserSettings user = _context.UserSettings.FirstOrDefault(u => u.User.Id == userId);
            if(user != null)
            {
                return user.LeagueId;
            }

            return 0;
        }

        public static SiteSettingsViewModel GetSiteSettings(ApplicationDbContext db, string userId)
        {
            var associationId = UserService.GetUserAssociationId(db, userId);
            var leagueId = RefereeServices.GetRefereeLeagueId(userId, db);
            var organizationId = OrganizationService.GetOrganizationId(db, associationId);
            var seasonId = SeasonServices.GetCurrentSeason(db, leagueId);

            SiteSettingsViewModel settings = new SiteSettingsViewModel()
            {
                UserId = userId,
                LeagueId = leagueId,
                OrganziationId = organizationId,
                SeasonId = seasonId,
                SportId = 1,
                AssociationId = associationId
            };

            return settings;
        }

        public static Profile GetProfile(ApplicationDbContext context, string userId)
        {
            return context.Profiles.AsNoTracking().FirstOrDefault(p => p.UserId == userId);
        }

        public static bool UpdateProfile(ApplicationDbContext context, Profile profile)
        {
            try
            {
                context.Update(profile);
                context.SaveChanges();
                return true;
            }
           catch (DbUpdateConcurrencyException)
            {
                return false;
            }
        }

    }
}
