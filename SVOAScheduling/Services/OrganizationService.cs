﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Services
{
    public class OrganizationService
    {
        public static int GetOrganizationId(ApplicationDbContext _context, int associationId)
        {
            return _context.Organizations.FirstOrDefault(o => o.AssociationId == associationId).Id;
        }
    }
}
