﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Services
{
    public class UserService
    {
        public static int GetUserAssociationId(ApplicationDbContext db, string userId)
        {
            return db.UserSettings.AsNoTracking().FirstOrDefault(us => us.User.Id == userId).AssociationId;
        }

        public static UserSettings GetUserSettings(ApplicationDbContext db, string userId)
        {
            return db.UserSettings.AsNoTracking().FirstOrDefault(us => us.User.Id == userId);
        }

      /*  public static string GetUserId()
        {
            return User.FindFirstValue(ClaimTypes.NameIdentifier);
        }
      */
    }
}
