﻿using System;
using System.Collections.Generic;
using System.Linq;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Services
{
    public class LocationServices
    {
        public static List<Location> GetLocations(ApplicationDbContext _context, int organizationId)
        {
            return _context.Locations.Where(l => l.OrganizationId == organizationId).ToList();
        }
    }
}
