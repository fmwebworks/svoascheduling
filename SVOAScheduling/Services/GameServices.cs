﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Data;
using SVOAScheduling.Models;
using SVOAScheduling.Models.ViewModels;

namespace SVOAScheduling.Services
{
    public class GameServices
    {
        public static List<Game> GetGamesBySeason(ApplicationDbContext db, int seasonId)
        {
            var games = db.Games
                .Include(g => g.Division)
                .Include(g => g.Format)
                .Include(g => g.Location)
                .Include(g => g.HomeTeam)
                .Include(g => g.VisitingTeam)
                .Include(g => g.Payment)
                .Include(g => g.Season)
                .Include(g => g.GameAssignments)
                .Include(g => g.GameAssignments).ThenInclude(ga => ga.Official)
                .Include(g => g.GameAssignments).ThenInclude(ga => ga.Type)
                .Where(g => g.SeasonId == seasonId && g.IsActive)
                .OrderBy(g => g.Date).ThenBy(g => g.LocationId).ToList();

            return games;

        }

        public static Game GetGameById(ApplicationDbContext context, int? gameId)
        {
            if(gameId is null)
            {
                return null;
            }

            var game = context.Games
                .Include(g => g.HomeTeam)
                .Include(g => g.Location).Include(g => g.VisitingTeam)
                .Include(g => g.GameAssignments).ThenInclude(ga => ga.Official)
                .Include(g => g.GameAssignments).ThenInclude(ga => ga.Type)
                .FirstOrDefault(g => g.Id == gameId);

            return (game is null) ? new Game() : game;
        }

        public static List<Game> GetAffiliatedGames(ApplicationDbContext context, Game game)
        {
            if(game is null)
            {
                return null;
            }

            var affiliated = context.Games.Include(g => g.Division).Include(g=> g.GameAssignments).ThenInclude(ga => ga.Official).Include(g => g.GameAssignments).ThenInclude(ga => ga.Type).Where(g => g.Date.Date == game.Date.Date && g.LocationId == game.LocationId && g.Id != game.Id).ToList();

            return (affiliated is null) ? new List<Game>() : affiliated;
        }

        public static GameViewModel PopulateGameViewModel(Game game, List<Game> affiliatedGames, bool selected )
        {
            GameViewModel match = new GameViewModel()
            {
                GameId = game.Id,
                Locaton = game.Location.Name,
                VisitingTeam = game.VisitingTeam.Abbreviation,
                HomeTeam = game.HomeTeam.Abbreviation,
                Date = game.Date,
                Checked = selected,
                GameNumber = game.Number,
                AffiliatedGames = new List<AffiliatedGameViewModel>(),
                Assignments = new List<AssignmentViewModel>()
            };
            if(game.GameAssignments.Count > 0 && match.Assignments.Count == 0)
            {
                foreach(var ga in game.GameAssignments)
                {
                    match.Assignments.Add(new AssignmentViewModel() {
                        AssignmentId = ga.Id,
                        AssignmentTypeId = ga.AssignmentTypeId,
                        AssignmentText = ga.Type.Description,
                        OfficialId = ga.OfficialId,
                        OfficialName = ga.Official.FullName,
                        SortOrder = ga.Type.SortOrder
                    });
                }
            }

            foreach (var items in affiliatedGames)
            {
                var affiliatedGame = new AffiliatedGameViewModel() { GameId = items.Id, Division = items.Division.Name, Checked = selected, Date = items.Date, GameNumber = items.Number, Assignments = new List<AssignmentViewModel>() };
                if (items.GameAssignments.Count() > 0)
                {
                    foreach (var assignment in items.GameAssignments)
                    {
                        var assignmentVM = new AssignmentViewModel()
                        {
                            AssignmentId = assignment.Id,
                            AssignmentTypeId = assignment.AssignmentTypeId,
                            AssignmentText = assignment.Type.Description,
                            OfficialId = assignment.OfficialId,
                            OfficialName = assignment.Official.FullName,
                            SortOrder = assignment.Type.SortOrder
                        };
                        if(assignmentVM != null)
                        {
                        affiliatedGame.Assignments.Add(assignmentVM);

                        }
                   }
           }
                      match.AffiliatedGames.Add(affiliatedGame);

            }

            return match;
        }

        public static List<Game> GetGamesByUser(ApplicationDbContext context, string userId)
        {
            return context.Games
                 .Include(g => g.Location)
                 .Include(g => g.HomeTeam)
                 .Include(g => g.VisitingTeam)
                 .Include(g => g.GameAssignments).ThenInclude(ga => ga.Official)
                 .Include(g => g.GameAssignments).ThenInclude(ga => ga.Type)
                 .Where(g => g.GameAssignments.Any(a => a.OfficialId == userId)).ToList();
        }

        public static bool GameExists(ApplicationDbContext context, int id)
        {
            return context.Games.Any(e => e.Id == id);
        }

    }
}
