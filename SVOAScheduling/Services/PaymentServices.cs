﻿using System;
using System.Linq;
using SVOAScheduling.Data;
using SVOAScheduling.Models;

namespace SVOAScheduling.Services
{
    public class PaymentServices
    {
        public static IQueryable<Payment> GetPayments(ApplicationDbContext _context, int leagueId)
        {
            return _context.Payments.Where(p => p.LeagueId == leagueId);
        }
    }
}
