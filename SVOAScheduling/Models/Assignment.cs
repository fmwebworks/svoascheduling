﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace SVOAScheduling.Models
{
    public class Assignment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [DisplayName("Official")]
        public string OfficialId { get; set; }
        public Profile Official { get; set; }
        public string Gym { get; set; }
        [DisplayName("Assignment")]
        public int AssignmentTypeId { get; set; }
        [DisplayName("Assignment")]
        public AssignmentType Type { get; set; }
        public int GameId { get; set; }
        public Game Game { get; set; }


        public Assignment()
        {
        }
    }
}
