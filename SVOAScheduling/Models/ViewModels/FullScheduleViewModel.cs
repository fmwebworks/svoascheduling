﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SVOAScheduling.Models.ViewModels
{
    public class FullScheduleViewModel
    {
        public int Id { get; set; }
        public string Location { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public string Division { get; set; }
        public string HomeTeam { get; set; }
        public string VisitingTeam { get; set; }
        public string Number { get; set; }
        public string R1 { get; set; }
        public string R2 { get; set; }

        public FullScheduleViewModel()
        {
        }
    }
}
