﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SVOAScheduling.Models;

namespace SVOAScheduling.Models.ViewModels
{
    public class myScheduleVM
    {
        public string Location { get; set; }
        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime MatchDate { get; set; }
        [DisplayName("Home Team")]
        public string HomeTeam { get; set; }
        public int MatchId { get; set; }
        [DisplayName("Number")]
        public string MatchNumber { get; set; }
        [DisplayName("Visiting Team")]
        public string VisitingTeam { get; set; }
        public string R1 { get; set; }
        public string R2 { get; set; }



        public myScheduleVM()
        {
        }
    }
}
