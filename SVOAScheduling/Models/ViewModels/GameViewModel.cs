﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SVOAScheduling.Models.ViewModels
{
    public class GameViewModel
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public string Gym { get; set; }
        public string Locaton { get; set; }
        public string VisitingTeam { get; set; }
        public string HomeTeam { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}")]
        public DateTime Date { get; set; }
        public string GameNumber { get; set; }
        public bool Checked { get; set; }

        public List<AffiliatedGameViewModel> AffiliatedGames { get; set; }

        public List<AssignmentViewModel> Assignments { get; set; }

        public string DateOnly => Date.ToString("dd-MMM-yy");
        public string TimeOnly => Date.ToString("H:mm");

        public GameViewModel()
        {
        }
    }
}
