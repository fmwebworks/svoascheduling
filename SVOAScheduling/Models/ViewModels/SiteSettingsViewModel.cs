﻿using System;
namespace SVOAScheduling.Models.ViewModels
{
    public class SiteSettingsViewModel
    {
        public string UserId { get; set; }
        public int LeagueId { get; set; }
        public int OrganziationId { get; set; }
        public int SeasonId { get; set; }
        public int SportId { get; set; }
        public int AssociationId { get; set; }


        public SiteSettingsViewModel()
        {
        }
    }
}
