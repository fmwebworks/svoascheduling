﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SVOAScheduling.Models.ViewModels
{
    public class AssignmentViewModel
    {
        public int AssignmentId { get; set; }
        public int AssignmentTypeId { get; set; }
        public string AssignmentText { get; set; }
        [DisplayName("Official")]
        public string OfficialId { get; set; }
        public string OfficialName { get; set; }
        public string Gym { get; set; }
        public int SortOrder { get; set; }

        public AssignmentViewModel()
        {
        }
    }
}
