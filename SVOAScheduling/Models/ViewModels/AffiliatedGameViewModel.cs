﻿using System;
using System.Collections.Generic;

namespace SVOAScheduling.Models.ViewModels
{
    public class AffiliatedGameViewModel
    {
        public int GameId { get; set; }
        public string Division { get; set; }
        public bool Checked { get; set; }
        public bool Reverse { get; set; }
        public string GameNumber { get; set; }
        public DateTime Date { get; set; }

        public List<AssignmentViewModel> Assignments { get; set; }

        public string TimeOnly => Date.ToString("H:mm");

        public AffiliatedGameViewModel()
        {
        }
    }
}
