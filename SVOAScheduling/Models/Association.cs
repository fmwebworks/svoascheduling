﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace SVOAScheduling.Models
{
    public class Association
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string PhoneNumber { get; set; }
        public int ProvincialAssociationId { get; set; }
        public ProvincialAssociation ProvincialAssociation { get; set; }

        public virtual ICollection<UserSettings> UserSettings { get; set; }

        public Association()
        {
        }
    }
}
      /*  public int RegionId { get; set; }
        public Region Region { get; set; }
      */
