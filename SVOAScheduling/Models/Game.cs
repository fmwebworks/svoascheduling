﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SVOAScheduling.Models
{
    public class Game
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [DisplayName("Location")]
        public int LocationId { get; set; }
        public Location Location { get; set; }
        [DisplayName("Season")]
        public int SeasonId { get; set; }
        public Season Season { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = false)]
        public DateTime Date { get; set; }
        [DisplayName("Payment")]
        public int? PaymentId { get; set; }
        public Payment Payment { get; set; }
        [DisplayName("Active")]
        public bool IsActive { get; set; }
        [DisplayName("Format")]
        public int? FormatId { get; set; }
        public Format Format { get; set; }
        [DisplayName("Division")]
        public int DivisionId { get; set; }
        public Division Division { get; set; }
        [DisplayName("Home Team")]
        public int? HomeTeamId { get; set; }
        public Location HomeTeam { get; set; }
        [DisplayName("Visiting Team")]
        public int? VisitingTeamId { get; set; }
        public Location VisitingTeam { get; set; }
        [DisplayName("Game #")]
        public string Number { get; set; }

        public ICollection<Assignment> GameAssignments { get; set; }

        public Game()
        {
        }
    }
}
