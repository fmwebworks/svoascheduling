﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SVOAScheduling.Models
{
    public class ProvincialAssociation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Term { get; set; }
        public string URL { get; set; }
        public int SportId { get; set; }
        public Sport Sport { get; set; }
        public int ProvinceId { get; set; }
        public Province Province { get; set; }
        public string Country { get; set; }

        public ProvincialAssociation()
        {
        }
    }
}
