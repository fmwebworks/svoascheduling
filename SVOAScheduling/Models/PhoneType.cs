﻿using System;
namespace SVOAScheduling.Models
{
    public class PhoneType
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public PhoneType()
        {
        }
    }
}
