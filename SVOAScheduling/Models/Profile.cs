﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace SVOAScheduling.Models
{
    public class Profile
    {
        [Key]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public IdentityUser User { get; set; }
        public int UserSettingsId { get; set; }
        public UserSettings UserSettings { get; set; }
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        [DisplayName("Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [DisplayFormat(DataFormatString = "{0:(###)###-####}", ApplyFormatInEditMode = true)]
        public string PhoneNumber { get; set; }
        [DisplayName("Phone Type")]
        public int PhoneTypeId { get; set; }
        public PhoneType PhoneType { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        [DisplayName("Province")]
        public int ProvinceId { get; set; }
        public Province Province { get; set; }
        [DisplayName("Postal Code")]
        [DataType(DataType.PostalCode)]
        [DisplayFormat(DataFormatString = "{0:*#* #*#}", ApplyFormatInEditMode = true)]
        public string PostalCode { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public Profile()
        {
        }
    }
}
