﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SVOAScheduling.Models
{
    public class Location
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Contact { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Abbreviation { get; set; }

        public Location()
        {
        }
    }
}
