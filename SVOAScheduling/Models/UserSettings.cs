﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace SVOAScheduling.Models
{
    public class UserSettings
    {
        [Key]
        public int Id { get; set; }
        public IdentityUser User { get; set; }
        public int AssociationId { get; set; }
        public Association Association { get; set; }
        public int LeagueId { get; set; }
        public League League { get; set; }
        public int SeasonId { get; set; }
        public Season Season { get; set; }
        public int LevelId { get; set; }
        public Level Level { get; set; }
        public bool Status { get; set; }

        public UserSettings()
        {
        }
    }
}
