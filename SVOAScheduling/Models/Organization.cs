﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace SVOAScheduling.Models
{
    public class Organization
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public int AssociationId { get; set; }
        public Association Association { get; set; }
        public string Contact { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }


        public virtual IEnumerable<League> Leagues { get; set; }

        public Organization()
        {
        }
    }
}
