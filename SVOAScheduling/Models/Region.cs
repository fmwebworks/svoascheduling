﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SVOAScheduling.Models
{
    public class Region
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public int ProvincialAssociationId { get; set; }
        public ProvincialAssociation ProvincialAssociation { get; set; }

        public Region()
        {
        }
    }
}
