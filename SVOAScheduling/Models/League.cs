﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace SVOAScheduling.Models
{
    public class League
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }
        public int LeagueTypeId { get; set; }
        public virtual LeagueType LeagueType { get; set; }

        public ICollection<UserSettings> UserSettings { get; set; }
        public virtual IEnumerable<Season> Seasons { get; set; }
        public virtual ICollection<Division> Divisions { get; set; }

        public League()
        {
        }
    }
}
