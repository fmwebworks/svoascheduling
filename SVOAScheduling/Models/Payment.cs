﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SVOAScheduling.Models
{
    public class Payment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int LeagueId { get; set; }
        public League League { get; set; }
        public int FormatId { get; set; }
        public Format Format { get; set; }
        public int Price { get; set; }

        public Payment()
        {
        }
    }
}
