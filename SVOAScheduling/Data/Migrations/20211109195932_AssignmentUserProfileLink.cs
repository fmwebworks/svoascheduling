﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SVOAScheduling.Data.Migrations
{
    public partial class AssignmentUserProfileLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_AspNetUsers_OfficialId",
                table: "Assignments");

            migrationBuilder.RenameColumn(
                name: "status",
                table: "UserSettings",
                newName: "Status");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "5097e227-4785-4138-84f7-efbb5a008128", "004cffee-4a76-4c8c-95b4-297efbb5a0b8" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a6b09d20-0c8a-47c0-b2bf-65e37c42d0ee", "6a164836-f5f1-4047-814a-c9c657a95c40" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "99d06622-c681-4cfe-974f-651f19a7d3f1", "f2d28cb4-13bf-4af4-9405-b088481a7de5" });

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_Profiles_OfficialId",
                table: "Assignments",
                column: "OfficialId",
                principalTable: "Profiles",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_Profiles_OfficialId",
                table: "Assignments");

            migrationBuilder.RenameColumn(
                name: "Status",
                table: "UserSettings",
                newName: "status");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "cf51ae9f-aec6-4cf2-8019-fd2239e199ee", "2ba88e19-d126-49f1-bd63-173c81be7068" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "597e8203-3816-4052-83c5-8edcbc1f2d3e", "242740d4-276c-4beb-8d07-9af89c53fb72" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "4941b3ee-0d01-4265-a932-009490ce5ba1", "5fbc5740-a106-4bcd-82cc-84be990753b1" });

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_AspNetUsers_OfficialId",
                table: "Assignments",
                column: "OfficialId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
