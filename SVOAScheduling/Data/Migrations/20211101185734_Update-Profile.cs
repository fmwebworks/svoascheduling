﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SVOAScheduling.Data.Migrations
{
    public partial class UpdateProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_AspNetUsers_UserId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_PhoneType_PhoneTypeId",
                table: "Profiles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PhoneType",
                table: "PhoneTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Profiles",
                table: "Profiles");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Profiles",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Profiles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserSettingsId",
                table: "Profiles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Profiles",
                table: "Profiles",
                column: "UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PhoneTypes",
                table: "PhoneTypes",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "cf51ae9f-aec6-4cf2-8019-fd2239e199ee", "2ba88e19-d126-49f1-bd63-173c81be7068" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "597e8203-3816-4052-83c5-8edcbc1f2d3e", "242740d4-276c-4beb-8d07-9af89c53fb72" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "4941b3ee-0d01-4265-a932-009490ce5ba1", "5fbc5740-a106-4bcd-82cc-84be990753b1" });

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_UserSettingsId",
                table: "Profiles",
                column: "UserSettingsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_AspNetUsers_UserId",
                table: "Profiles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_PhoneTypes_PhoneTypeId",
                table: "Profiles",
                column: "PhoneTypeId",
                principalTable: "PhoneTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_UserSettings_UserSettingsId",
                table: "Profiles",
                column: "UserSettingsId",
                principalTable: "UserSettings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_AspNetUsers_UserId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_PhoneTypes_PhoneTypeId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_UserSettings_UserSettingsId",
                table: "Profiles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Profiles",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_UserSettingsId",
                table: "Profiles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PhoneTypes",
                table: "PhoneTypes");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "UserSettingsId",
                table: "Profiles");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Profiles",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PhoneTypes",
                table: "PhoneTypes",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a6f6efbf-db66-4173-b971-f5e3a3803295", "1d4baabc-a2b3-4313-8d25-123b22558911" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "48a324f1-c624-444c-8d2c-253c1e09b1f1", "300d69b1-d045-4f22-b5f2-984569a891c1" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "c4d0556d-d151-4544-a4a9-e61bf99a0786", "e30ecf5a-dcbd-4499-8ba7-a05e807b24fe" });

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_AspNetUsers_UserId",
                table: "Profiles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_PhoneTypes_PhoneTypeId",
                table: "Profiles",
                column: "PhoneTypeId",
                principalTable: "PhoneTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
