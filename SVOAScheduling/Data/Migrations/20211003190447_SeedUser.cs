﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SVOAScheduling.Data.Migrations
{
    public partial class SeedUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM [AspNetRoles]", true);
            migrationBuilder.Sql("DELETE FROM [AspNetUsers]", true);
            migrationBuilder.Sql("DELETE FROM [Format]", true);
            migrationBuilder.Sql("DELETE FROM [AspNetUserRoles]", true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "d13170a9-3e55-451e-b875-ecfbedb564e4", "1", "Admin", "Admin" },
                    { "65bf01d2-cfe9-45f5-acf7-86486b80c419", "2", "Assignor", "Assignor" },
                    { "685e668b-18d3-448a-b26b-39bf7ccd0527", "3", "Referee", "Referee" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "fd618e66-d514-4ce0-bc2b-607165dfd016", 0, "24f79627-89ad-4e75-bd91-885a6a429241", "melduss@hotmail.com", true, false, null, null, null, null, "1234567890", false, "1e4d53a4-637f-478c-8f38-a9843584b1c0", false, "melduss@hotmail.com" },
                    { "51de36f0-bf1b-40a2-b280-d3fe36fcd404", 0, "1aa5573b-dd97-4141-b817-27c5de38750c", "claudette@gmail.com", true, false, null, null, null, null, "1234567890", false, "edccaba2-863b-4971-9c2c-d9ce1345d0bc", false, "claudette@gmail.com" },
                    { "a5979621-de89-4ba0-be91-ae218738ef71", 0, "8f0d42fc-7f92-4920-949f-4bea52ce02ca", "brian@gmail.com", true, false, null, null, null, null, "1234567890", false, "2c217789-b7f0-4cd1-8c53-f6b8dd7e035f", false, "brian@gmail.com" }
                });

            migrationBuilder.InsertData(
                table: "Format",
                columns: new[] { "Id", "Description" },
                values: new object[,]
                {
                    { 1, "2 Sets" },
                    { 2, "2/3 Sets" },
                    { 3, "3 Sets" },
                    { 4, "3/5 Sets" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "d13170a9-3e55-451e-b875-ecfbedb564e4", "fd618e66-d514-4ce0-bc2b-607165dfd016" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "65bf01d2-cfe9-45f5-acf7-86486b80c419", "51de36f0-bf1b-40a2-b280-d3fe36fcd404" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "685e668b-18d3-448a-b26b-39bf7ccd0527", "a5979621-de89-4ba0-be91-ae218738ef71" });

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "EmailIndex",
                table: "AspNetUsers");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "65bf01d2-cfe9-45f5-acf7-86486b80c419", "51de36f0-bf1b-40a2-b280-d3fe36fcd404" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "685e668b-18d3-448a-b26b-39bf7ccd0527", "a5979621-de89-4ba0-be91-ae218738ef71" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "d13170a9-3e55-451e-b875-ecfbedb564e4", "fd618e66-d514-4ce0-bc2b-607165dfd016" });

            migrationBuilder.DeleteData(
                table: "Format",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Format",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Format",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Format",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "65bf01d2-cfe9-45f5-acf7-86486b80c419");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "685e668b-18d3-448a-b26b-39bf7ccd0527");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d13170a9-3e55-451e-b875-ecfbedb564e4");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016");
        }
    }
}
