﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SVOAScheduling.Data.Migrations
{
    public partial class UpdateAssignmentTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "284f64eb-9813-4966-9ce1-6906a5045919", "24888e8e-abc2-4e30-afa8-8b3a43a45868" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "ce2a6cea-d5da-4f95-8503-151c8bacf58a", "2e944a43-1ce0-4b1e-845a-20ddb4b69695" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f1030c43-f2a4-4027-9fb0-a3259e22904e", "40023fdf-cb00-4e94-9c5c-aacb2a383f35" });

            migrationBuilder.AddColumn<int>(
                name: "SortOrder",
                table: "AssignmentTypes",
                type: "int",
                nullable: true
                );

                migrationBuilder.AddColumn<int>(
                name: "SportId",
                table: "AssignmentTypes",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AssignmentTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "SortOrder", "SportId" },
                values: new object[] { 1, 1 });

            migrationBuilder.UpdateData(
                table: "AssignmentTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "SortOrder", "SportId" },
                values: new object[] { 2, 1 });

            migrationBuilder.UpdateData(
                table: "AssignmentTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "SortOrder", "SportId" },
                values: new object[] { 3, 1 });

            migrationBuilder.UpdateData(
                   table: "AssignmentTypes",
                   keyColumn: "Id",
                   keyValue: 4,
                   columns: new[] { "SortOrder", "SportId" },
                   values: new object[] { 4, 1 });

            migrationBuilder.UpdateData(
                table: "AssignmentTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "SortOrder", "SportId" },
                values: new object[] { 5, 1 });

            migrationBuilder.UpdateData(
                    table: "AssignmentTypes",
                    keyColumn: "Id",
                    keyValue: 6,
                    columns: new[] { "SortOrder", "SportId" },
                    values: new object[] { 6, 1 });

            migrationBuilder.UpdateData(
                   table: "AssignmentTypes",
                   keyColumn: "Id",
                   keyValue: 7,
                   columns: new[] { "SortOrder", "SportId" },
                   values: new object[] { 7, 1 });

            migrationBuilder.UpdateData(
                   table: "AssignmentTypes",
                   keyColumn: "Id",
                   keyValue: 8,
                   columns: new[] { "SortOrder", "SportId" },
                   values: new object[] { 8, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SortOrder",
                table: "AssignmentTypes");

            migrationBuilder.DropColumn(
                name: "SportId",
                table: "AssignmentTypes");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "2592f43a-3c48-4559-a511-a8282e26377a", "58fa5d71-7674-4ff8-931d-13369fd0566d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "87e7af13-117f-40c6-8df0-7b495dc948f7", "fa5f2c58-2a0e-4626-9132-7f7308dd01ea" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "810fdfdd-fa8d-4f55-831f-3739a356e5b9", "7fc9a01c-e87e-4b99-ab72-87d3814f3984" });
        }
    }
}
