﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SVOAScheduling.Data.Migrations
{
    public partial class AssignmentGameChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssignmentGame");

            migrationBuilder.AddColumn<int>(
                name: "GameId",
                table: "Assignments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "cbaa2142-c91d-4cb5-9aeb-19270b6abb1f", "abe84ccc-989f-4301-a8fd-0d0283c42b95" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "6f1231bf-eda7-47e9-b1b7-bc0ba9d4ac66", "9cd68512-283c-4b3e-b26b-b447ca460562" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "9365ae7a-3b4e-475e-9d1e-363201eb735f", "745b2ca8-0ec6-47a9-8a04-5229da758e98" });

            migrationBuilder.CreateIndex(
                name: "IX_Assignments_GameId",
                table: "Assignments",
                column: "GameId");

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_Games_GameId",
                table: "Assignments",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_Games_GameId",
                table: "Assignments");

            migrationBuilder.DropIndex(
                name: "IX_Assignments_GameId",
                table: "Assignments");

            migrationBuilder.DropColumn(
                name: "GameId",
                table: "Assignments");

            migrationBuilder.CreateTable(
                name: "AssignmentGame",
                columns: table => new
                {
                    AssignmentGamesId = table.Column<int>(type: "int", nullable: false),
                    GameAssignmentsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignmentGame", x => new { x.AssignmentGamesId, x.GameAssignmentsId });
                    table.ForeignKey(
                        name: "FK_AssignmentGame_Assignments_GameAssignmentsId",
                        column: x => x.GameAssignmentsId,
                        principalTable: "Assignments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssignmentGame_Games_AssignmentGamesId",
                        column: x => x.AssignmentGamesId,
                        principalTable: "Games",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "7f89fe37-759a-4c8c-85ab-6dd631fb1835", "77bb54ca-8598-4aa6-9d46-43bd0e3bc324" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "673f17eb-a3a6-4a56-8d71-6e72958fd9ca", "19005372-67ad-4a8d-88b3-25d078e7e497" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "20a1c12a-0ff2-429b-9561-0ebd2a11e0ed", "584d5fd4-68ce-4605-aeb6-fbe57cec83ea" });

            migrationBuilder.CreateIndex(
                name: "IX_AssignmentGame_GameAssignmentsId",
                table: "AssignmentGame",
                column: "GameAssignmentsId");
        }
    }
}
