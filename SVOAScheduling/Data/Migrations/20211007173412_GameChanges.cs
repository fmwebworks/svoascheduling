﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SVOAScheduling.Data.Migrations
{
    public partial class GameChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssignmentGame_Assignments_AssignmentsId",
                table: "AssignmentGame");

            migrationBuilder.DropForeignKey(
                name: "FK_AssignmentGame_Games_GamesId",
                table: "AssignmentGame");

            migrationBuilder.RenameColumn(
                name: "GamesId",
                table: "AssignmentGame",
                newName: "GameAssignmentsId");

            migrationBuilder.RenameColumn(
                name: "AssignmentsId",
                table: "AssignmentGame",
                newName: "AssignmentGamesId");

            migrationBuilder.RenameIndex(
                name: "IX_AssignmentGame_GamesId",
                table: "AssignmentGame",
                newName: "IX_AssignmentGame_GameAssignmentsId");

            migrationBuilder.AddColumn<int>(
                name: "HomeTeamId",
                table: "Games",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Number",
                table: "Games",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VisitingTeamId",
                table: "Games",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "7f89fe37-759a-4c8c-85ab-6dd631fb1835", "77bb54ca-8598-4aa6-9d46-43bd0e3bc324" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "673f17eb-a3a6-4a56-8d71-6e72958fd9ca", "19005372-67ad-4a8d-88b3-25d078e7e497" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "20a1c12a-0ff2-429b-9561-0ebd2a11e0ed", "584d5fd4-68ce-4605-aeb6-fbe57cec83ea" });

            migrationBuilder.UpdateData(
                table: "Leagues",
                keyColumn: "Id",
                keyValue: 1,
                column: "OrganizationId",
                value: 1);

            migrationBuilder.CreateIndex(
                name: "IX_Games_HomeTeamId",
                table: "Games",
                column: "HomeTeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Games_VisitingTeamId",
                table: "Games",
                column: "VisitingTeamId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssignmentGame_Assignments_GameAssignmentsId",
                table: "AssignmentGame",
                column: "GameAssignmentsId",
                principalTable: "Assignments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssignmentGame_Games_AssignmentGamesId",
                table: "AssignmentGame",
                column: "AssignmentGamesId",
                principalTable: "Games",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Locations_HomeTeamId",
                table: "Games",
                column: "HomeTeamId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Locations_VisitingTeamId",
                table: "Games",
                column: "VisitingTeamId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssignmentGame_Assignments_GameAssignmentsId",
                table: "AssignmentGame");

            migrationBuilder.DropForeignKey(
                name: "FK_AssignmentGame_Games_AssignmentGamesId",
                table: "AssignmentGame");

            migrationBuilder.DropForeignKey(
                name: "FK_Games_Locations_HomeTeamId",
                table: "Games");

            migrationBuilder.DropForeignKey(
                name: "FK_Games_Locations_VisitingTeamId",
                table: "Games");

            migrationBuilder.DropIndex(
                name: "IX_Games_HomeTeamId",
                table: "Games");

            migrationBuilder.DropIndex(
                name: "IX_Games_VisitingTeamId",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "HomeTeamId",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "Number",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "VisitingTeamId",
                table: "Games");

            migrationBuilder.RenameColumn(
                name: "GameAssignmentsId",
                table: "AssignmentGame",
                newName: "GamesId");

            migrationBuilder.RenameColumn(
                name: "AssignmentGamesId",
                table: "AssignmentGame",
                newName: "AssignmentsId");

            migrationBuilder.RenameIndex(
                name: "IX_AssignmentGame_GameAssignmentsId",
                table: "AssignmentGame",
                newName: "IX_AssignmentGame_GamesId");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "e7d42041-7ee1-419a-9f68-a1a2a2d16748", "1094ba1e-7db9-4ecd-baa0-45ef504a45ab" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "1d08c791-72f5-46ab-99c2-866c5ad1e2d4", "69e42261-4741-4a04-8b5d-7a492cd4f544" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "8aec36e9-ca54-40b9-89a8-2f3ad0df8487", "72d24621-0bc8-4999-a9d5-cd4f89d1c937" });

            migrationBuilder.UpdateData(
                table: "Leagues",
                keyColumn: "Id",
                keyValue: 1,
                column: "OrganizationId",
                value: 2);

            migrationBuilder.AddForeignKey(
                name: "FK_AssignmentGame_Assignments_AssignmentsId",
                table: "AssignmentGame",
                column: "AssignmentsId",
                principalTable: "Assignments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssignmentGame_Games_GamesId",
                table: "AssignmentGame",
                column: "GamesId",
                principalTable: "Games",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
