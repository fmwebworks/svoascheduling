﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SVOAScheduling.Data.Migrations
{
    public partial class UserProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Leagues_LeagueId",
                table: "Locations");

            migrationBuilder.DropIndex(
                name: "IX_Locations_LeagueId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "LeagueId",
                table: "Locations");

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "UserSettings",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "PhoneTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoneType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Profiles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneTypeId = table.Column<int>(type: "int", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProvinceId = table.Column<int>(type: "int", nullable: false),
                    PostalCode = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profiles", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Profiles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Profiles_PhoneType_PhoneTypeId",
                        column: x => x.PhoneTypeId,
                        principalTable: "PhoneType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Profiles_Provinces_ProvinceId",
                        column: x => x.ProvinceId,
                        principalTable: "Provinces",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "a6f6efbf-db66-4173-b971-f5e3a3803295", "1d4baabc-a2b3-4313-8d25-123b22558911" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "48a324f1-c624-444c-8d2c-253c1e09b1f1", "300d69b1-d045-4f22-b5f2-984569a891c1" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "c4d0556d-d151-4544-a4a9-e61bf99a0786", "e30ecf5a-dcbd-4499-8ba7-a05e807b24fe" });

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_PhoneTypeId",
                table: "Profiles",
                column: "PhoneTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_ProvinceId",
                table: "Profiles",
                column: "ProvinceId");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Profiles");

            migrationBuilder.DropTable(
                name: "PhoneType");

            migrationBuilder.DropColumn(
                name: "status",
                table: "UserSettings");

            migrationBuilder.AddColumn<int>(
                name: "LeagueId",
                table: "Locations",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "284f64eb-9813-4966-9ce1-6906a5045919", "24888e8e-abc2-4e30-afa8-8b3a43a45868" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a5979621-de89-4ba0-be91-ae218738ef71",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "ce2a6cea-d5da-4f95-8503-151c8bacf58a", "2e944a43-1ce0-4b1e-845a-20ddb4b69695" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "fd618e66-d514-4ce0-bc2b-607165dfd016",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f1030c43-f2a4-4027-9fb0-a3259e22904e", "40023fdf-cb00-4e94-9c5c-aacb2a383f35" });

            migrationBuilder.CreateIndex(
                name: "IX_Locations_LeagueId",
                table: "Locations",
                column: "LeagueId");

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Leagues_LeagueId",
                table: "Locations",
                column: "LeagueId",
                principalTable: "Leagues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
