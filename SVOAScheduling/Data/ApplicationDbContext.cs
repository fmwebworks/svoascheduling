using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SVOAScheduling.Models;

namespace SVOAScheduling.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Format> Format { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<AssignmentType> AssignmentTypes { get; set; }
        public DbSet<Association> Associations { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<League> Leagues { get; set; }
        public DbSet<LeagueType> LeagueTypes { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<ProvincialAssociation> ProvincialAssociations { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<Sport> Sports { get; set; }
        public DbSet<UserSettings> UserSettings { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<PhoneType> PhoneTypes { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            SeedRoles(builder);
            SeedUsers(builder);
            SeedUserRoles(builder);
            SeedFormat(builder);
            SeedProvince(builder);
            SeedSport(builder);
            SeedProvincialAssociation(builder);
            SeedAssociation(builder);
            SeedLeagueType(builder);
            SeedDivision(builder);
            SeedSeason(builder);
            SeedRegion(builder);
            SeedOrganization(builder);
            SeedLocation(builder);
            SeedLeague(builder);
            SeedAssignmentTypes(builder);
            
        }

        private void SeedRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityRole>().HasData(
                new IdentityRole() { Id = "d13170a9-3e55-451e-b875-ecfbedb564e4", Name = "Admin", ConcurrencyStamp = "1", NormalizedName = "Admin" },
                new IdentityRole() { Id = "65bf01d2-cfe9-45f5-acf7-86486b80c419", Name = "Assignor", ConcurrencyStamp = "2", NormalizedName = "Assignor" },
                new IdentityRole() { Id = "685e668b-18d3-448a-b26b-39bf7ccd0527", Name = "Referee", ConcurrencyStamp = "3", NormalizedName = "Referee" }
                );
        }

        private void SeedUsers(ModelBuilder builder)
        {
            IdentityUser user = new IdentityUser()
            {
                Id = "fd618e66-d514-4ce0-bc2b-607165dfd016",
                UserName = "melduss@hotmail.com",
                Email = "melduss@hotmail.com",
                LockoutEnabled = false,
                PhoneNumber = "1234567890",
                EmailConfirmed = true
            };

            PasswordHasher<IdentityUser> passwordHasher = new PasswordHasher<IdentityUser>();
            passwordHasher.HashPassword(user, "ReP86WO'x84;0NA");

            IdentityUser user2 = new IdentityUser()
            {
                Id = "51de36f0-bf1b-40a2-b280-d3fe36fcd404",
                UserName = "claudette@gmail.com",
                Email = "claudette@gmail.com",
                LockoutEnabled = false,
                PhoneNumber = "1234567890",
                EmailConfirmed = true
            };

            passwordHasher.HashPassword(user2, "fs9F($x1a8WS70m");

            IdentityUser user3 = new IdentityUser()
            {
                Id = "a5979621-de89-4ba0-be91-ae218738ef71",
                UserName = "brian@gmail.com",
                Email = "brian@gmail.com",
                LockoutEnabled = false,
                PhoneNumber = "1234567890",
                EmailConfirmed = true
            };

            passwordHasher.HashPassword(user3, "P82dj91xGI-a$6e");


            List<IdentityUser> users = new List<IdentityUser> { };
            users.Add(user);
            users.Add(user2);
            users.Add(user3);
           
            
            builder.Entity<IdentityUser>().HasData(users);
        }

        private void SeedUserRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityUserRole<string>>().HasData( new List<IdentityUserRole<string>> {
                new IdentityUserRole<string>() { RoleId = "d13170a9-3e55-451e-b875-ecfbedb564e4", UserId = "fd618e66-d514-4ce0-bc2b-607165dfd016" },
                new IdentityUserRole<string>() { RoleId = "65bf01d2-cfe9-45f5-acf7-86486b80c419", UserId = "51de36f0-bf1b-40a2-b280-d3fe36fcd404" },
                new IdentityUserRole<string>() { RoleId = "685e668b-18d3-448a-b26b-39bf7ccd0527", UserId = "a5979621-de89-4ba0-be91-ae218738ef71" }
                }
            );
        }

        private void SeedFormat(ModelBuilder builder)
        {
            builder.Entity<Format>().HasData(
                new Format { Id = 1, Description = "2 Sets" },
                new Format { Id = 2, Description = "2/3 Sets" },
                new Format { Id = 3, Description = "3 Sets" },
                new Format { Id = 4, Description = "3/5 Sets" });
        }

        private void SeedLeagueType(ModelBuilder builder)
        {
            builder.Entity<LeagueType>().HasData(
                new LeagueType { Id = 1, Description = "Season" },
                new LeagueType { Id = 2, Description = "Tournaments" },
                new LeagueType { Id = 3, Description = "Playoffs" }
                );
        }

        private void SeedSport(ModelBuilder builder)
        {
            builder.Entity<Sport>().HasData(
                new Sport { Id = 1, Description = "Volleyball" }
                );
        }

        private void SeedProvince(ModelBuilder builder)
        {
            builder.Entity<Province>().HasData(
                new Province { Id = 1, Term = "Ontario", Abbreviation = "ON" },
                new Province { Id = 2, Term = "Quebec", Abbreviation = "QC" }
                );
        }

        private void SeedProvincialAssociation(ModelBuilder builder)
        {
            builder.Entity<ProvincialAssociation>().HasData(
                new ProvincialAssociation { Id = 1, Term = "OVA", ProvinceId = 1, SportId = 1, Country = "Canada", URL = "http://www.ontariovolleyball.org/" }
                );
        }

        private void SeedLocation(ModelBuilder builder)
        {
            builder.Entity<Location>().HasData(
                new Location { Id = 1, Name = "Lasalle Secondary School", Abbreviation = "LSS", City = "Sudbury", Contact = "Kelly P", PhoneNumber = "705.524.3654", Address = "Kennedy", OrganizationId = 2 },
                new Location { Id = 2, Name = "Lo-Ellen Park Secondary School", Abbreviation = "LEP", City = "Sudbury", Contact = "Sherry Green", PhoneNumber = "705.522.2345", Address = "275 Loachs Rd", OrganizationId = 2 },
                new Location { Id = 3, Name = "St. Charles Secondary School", Abbreviation = "SCS", City = "Sudbury", Contact = "Dwain P", PhoneNumber = "705.524.1234", Address = "Falconbridge", OrganizationId = 2 },
                new Location { Id = 4, Name = "St. Benedicts Secondary School", Abbreviation = "SBS", City = "Sudbury", Contact = "Carl P", PhoneNumber = "705.522.8764", Address = "Algonquin", OrganizationId = 2 });
        }

        private void SeedOrganization(ModelBuilder builder)
        {
            builder.Entity<Organization>().HasData(
                new Organization { Id = 1, AssociationId = 1, Name = "SDSSAA", Contact = "Dave Mackela", Email = "dave.makela@rainbowschools.ca" }
                );
        }

        private void SeedAssociation(ModelBuilder builder)
        {
            builder.Entity<Association>().HasData(
                new Association { Id = 1, Name = "SVOA", Contact = "Melanie Dussiaume", PhoneNumber = "7055551234", ProvincialAssociationId = 1 }
                );
        }

        private void SeedSeason(ModelBuilder builder)
        {
            builder.Entity<Season>().HasData(
                new Season { Id = 1, Name = "2021-2022", StartDate = Convert.ToDateTime("09/10/2021 08:00"), EndDate = Convert.ToDateTime("11/12/2021 21:00"), IsCurrent = true, LeagueId = 1 }
                );
        }

        private void SeedRegion(ModelBuilder builder)
        {
            builder.Entity<Region>().HasData(
                new Region { Id = 1, Name = "Region 1", ProvincialAssociationId = 1 },
                new Region { Id = 2, Name = "Region 2", ProvincialAssociationId = 1 },
                new Region { Id = 3, Name = "Region 3", ProvincialAssociationId = 1 },
                new Region { Id = 4, Name = "Region 4", ProvincialAssociationId = 1 },
                new Region { Id = 5, Name = "Region 5", ProvincialAssociationId = 1 },
                new Region { Id = 6, Name = "Region 6", ProvincialAssociationId = 1 }
                );
        }

        private void SeedDivision(ModelBuilder builder)
        {
            builder.Entity<Division>().HasData(
                new Division { Id = 1, Name = "MG", LeagueId = 1 },
                new Division { Id = 2, Name = "JR", LeagueId = 1 },
                new Division { Id = 3, Name = "SR", LeagueId = 1 },
                new Division { Id = 4, Name = "12UG", LeagueId = 2 },
                new Division { Id = 5, Name = "13UG", LeagueId = 2 },
                new Division { Id = 6, Name = "14UG", LeagueId = 2 },
                new Division { Id = 7, Name = "15UG", LeagueId = 2 },
                new Division { Id = 8, Name = "16UG", LeagueId = 2 },
                new Division { Id = 9, Name = "17UG", LeagueId = 2 },
                new Division { Id = 10, Name = "18UG", LeagueId = 2 },
                new Division { Id = 11, Name = "12UB", LeagueId = 2 },
                new Division { Id = 12, Name = "13UB", LeagueId = 2 },
                new Division { Id = 13, Name = "14UB", LeagueId = 2 },
                new Division { Id = 14, Name = "15UB", LeagueId = 2 },
                new Division { Id = 15, Name = "16UB", LeagueId = 2 },
                new Division { Id = 16, Name = "17UB", LeagueId = 2 },
                new Division { Id = 17, Name = "18UB", LeagueId = 2 }
                );
        }

        private static void SeedLeague(ModelBuilder builder)
        {
            builder.Entity<League>().HasData(
               new League { Id = 1, Name = "Boys", LeagueTypeId = 1, OrganizationId = 1 }
                );  
        }

        private void SeedAssignmentTypes(ModelBuilder builder)
        {
            builder.Entity<AssignmentType>().HasData(
                new AssignmentType { Id = 1, Description = "R1", SortOrder = 1, SportId = 1 },
                new AssignmentType { Id = 2, Description = "R2", SortOrder = 2, SportId = 1 },
                new AssignmentType { Id = 3, Description = "L1", SortOrder = 3, SportId = 1 },
                new AssignmentType { Id = 4, Description = "L2", SortOrder = 4, SportId = 1 },
                new AssignmentType { Id = 5, Description = "L3", SortOrder = 5, SportId = 1 },
                new AssignmentType { Id = 6, Description = "L4", SortOrder = 6, SportId = 1 },
                new AssignmentType { Id = 7, Description = "Scorer", SortOrder = 7, SportId = 1 },
                new AssignmentType { Id = 8, Description = "Assistant Scorer", SortOrder = 8, SportId = 1 }
                );
        }

    }
}
